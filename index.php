<html>
<head>
    <meta charset="UTF-8">
    <title>Angular Test</title>

    <script src="static/js/libs/jquery.js"></script>
    <script src="static/js/libs/jquery-ui.js"></script>
    <script src="static/js/libs/angular.js"></script>
    <script src="static/js/libs/angular-sanitize.js"></script>
    <script src="static/js/libs/angular-checklist-model.js"></script>
    <script src="static/js/libs/sortable.js"></script>
    <script src="static/js/libs/angularjs-datetime-picker.js"></script>

    <script src="static/js/libs/angular-translate.js"></script>
    <script src="static/js/libs/angular-translate-loader-static-files.js"></script>
    <script src="static/js/libs/angular-cookies-v1.4.0-beta.5.js"></script>
    <script src="static/js/libs/angular-translate-storage-local.js"></script>
    <script src="static/js/libs/angular-translate-storage-cookie.js"></script>

    <script src="static/js/namespace.js"></script>
    <script src="static/js/globals/constants/constants.js"></script>
    <script src="static/js/globals/controllers/common.js"></script>
    <script src="static/js/globals/helpers/http-util.js"></script>

    <script src="static/js/globals/providers/get-products.js"></script>

    <script src="static/js/apps/products/controllers/productList.js"></script>



</head>
<body>
<div ng-app="test.page" >
    <div ng-controller="commonCtrl">
        <div ng-controller="productCTRL">
            <div ng-init="getProducts(1)">
                <input type="text" class="form-control" ng-model="query">
                <table border="1">
                    <thead>
                    <tr>
                        <th><a href="" ng-click="sortField = 'name'; reverse = !reverse">Name</a></th>
                        <th><a href="" ng-click="sortField = 'code'; reverse = !reverse">Code</a></th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tr ng-repeat="product in products_bucket | orderBy:sortField:reverse | filter: query">
                        <td>{{ product.name }}</td>
                        <td>{{ product.code }}</td>
                        <td><a ng-href="{{ product.id }}">Edit</a></td>
                    </tr>
                </table>
                <button class="btn btn-success" ng-click="prev()">Prev</button>
                <button class="btn btn-success" ng-click="next()">Next</button>
            </div>
        </div>
    </div>
</div>
</body>
</html>