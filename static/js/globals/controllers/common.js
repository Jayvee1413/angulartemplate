(function()
{
    'use strict';
    
    angular.module('test.common').controller('commonCtrl', [
        '$scope',
        '$q',
        function(
            scope,
            promise
        )
        {
            scope.deferred = [];

            scope.getData = function(requestList) {
                var promises = [];
                
                angular.forEach(requestList, function(request) {
                    promises.push(scope.getPromise(request));
                });
                
                return promise.all(promises);
            };
            
            scope.getPromise = function(request) {
                var promiseIdx = scope.deferred.length,
                    params = (request.params) ? request.params : {};
                
                scope.deferred.push(promise.defer());
                
                request.provider(params, function(data, status, headers, config) {
                    return scope.deferred[promiseIdx].resolve({
                        'data' : data,
                        'status' : status,
                        'headers' : headers,
                        'config' : config
                    });
                }, function(data, status, headers, config) {
                    return scope.deferred[promiseIdx].reject({
                        'data' : data,
                        'status' : status,
                        'headers' : headers,
                        'config' : config
                    });
                });
                
                return scope.deferred[promiseIdx].promise;
            };

            scope.setUrlPath = function(dest){
                var base = window.location.origin;
                var path = window.location.pathname;

                path = path.split('/');
                path[path.length-1] = dest + '.html';

                if (dest == '/')
                {
                    path.splice(-1, 1);
                    path = path.join('/') + '/';
                } else
                {
                    path = path.join('/');
                }

                window.location.href = base + path;
            };
            
            scope.startIdleTimeout = function()
            {
                var interval = null,
                    idleTime = 0;

                var resetTimer = function(){
                    idleTime = 0;
                };

                var incrementTimer = function()
                {
                    idleTime = idleTime + 1;
                    if (idleTime > 29)
                    {
                        clearInterval(interval);
                        scope.setUrlPath('/');
                    }
                };

                var executeInterval = function()
                {
                    interval = setInterval(incrementTimer, 60000);
                };

                window.onclick = resetTimer;
                window.onmousemove = resetTimer;
                window.onmousedown = resetTimer;
                window.onkeydown = resetTimer;
                window.onkeyup = resetTimer;
                window.onkeypress = resetTimer;

                executeInterval();
            };

        }
    ]);
}());
