(function () {
    'use strict';

    angular.module('test.data').factory('getProductsProvider', [
        'httpUtil',
        '$q',
        'productsProviderUrl',
        function(
            httpUtil,
            $q,
			productsProviderUrl
        ) {
            var self = this;
            
            self.canceller = null;

            self.getData = function(params, callback, errorCallback) {
                self.canceller = $q.defer();
                
				var routeToUse = productsProviderUrl;
				
                httpUtil({
                    'url' : routeToUse,
                    'method' : 'GET',
                    'data' : params,
                    'config' : {
                        'timeout' : self.canceller.promise
                    },
                    'success' : function(response, status, headers, config) {
                        callback(self.dataManager(response), status, headers, config);
                    },
                    'error' : function(response, status, headers, config) {
                        errorCallback(response, status, headers, config);
                    }
                });
            };

            self.dataManager = function(response) {
                var managedData = {
                    'response_code' : response.status,
                    'response_desc' : response.message,
                    'data' : []
                };
				if (response.results.data && response.results.data.length > 0)
				{
					angular.forEach(response.results.data, function(val, idx)
					{
						var objKeys = Object.keys(val),
	                        objData = {};

	                    angular.forEach(objKeys, function(keyVal, keyIdx)
	                    {
	                        objData[keyVal] = val[keyVal];
	                    });

	                    managedData.data.push(objData);

					});
				
				} else
				{
					var data = response.data;
					
					if (data && Object.keys(data).length > 0)
					{
						var objKeys = Object.keys(data),
	                        objData = {};

	                    angular.forEach(objKeys, function(keyVal, keyIdx)
	                    {
	                        objData[keyVal] = data[keyVal];
	                    });

	                    managedData.data.push(objData);
					}
				}
                return managedData;
            };
            
            self.cancelLastCall = function() {
                self.canceller.resolve();
            };

            return {
                'getData' : self.getData,
                'cancelLastCall' : self.cancelLastCall
            };
        }
    ]);
})();