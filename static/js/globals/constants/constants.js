(function() {
    'use strict';
    var HOST = "http://localhost:8888";
        
    angular.module('test.data')
        .value('globalTemplatesPath', 'static/js/globals/templates/')
        .value('productsProviderUrl', HOST + '/storm/store/v1/products')
        ;
}());