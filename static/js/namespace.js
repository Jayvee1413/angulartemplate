(function()
{
	'use strict';
	
	angular.module('test.common', []);
	angular.module('test.data', []);
	
	angular.module('test.page', [
		'test.common',
		'test.data'
	]);
}());