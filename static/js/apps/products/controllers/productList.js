/**
 * Created by arc on 21/04/2016.
 */
(function()
{
    'use strict';

    angular.module('test.page').controller('productCTRL', [
        '$scope',
        'getProductsProvider',
        function(
            scope,
            getProductsProvider
        ){
            scope.products_bucket = [];
            scope.products_current_page = 1;
            scope.query = "";

            scope.getProducts = function(page_number)
            {
                if (page_number < 1)
                    page_number = 1;
                scope.products_current_page = page_number;
                scope.processProducts = function (params){
                    if(params.length < 1){
                        scope.getProductsPrev();
                    }
                    scope.products_bucket = params;
                };
                scope.getData([
                    {
                        'provider' : getProductsProvider.getData,
                        'params' : {
                            'page': page_number
                        }
                    }
                ]).then(function(responses)
                {
                    scope.processProducts(responses[0].data.data);

                }, function(errorResponse)
                {
                    // Handle Error
                    console.log(errorResponse);
                });

            };

            scope.next = function(){
                scope.getProducts(scope.products_current_page + 1);
            };

            scope.prev = function(){
                scope.getProducts(scope.products_current_page - 1);
            };

            scope.sort = {
                sortingOrder : 'id',
                reverse : false
            };
        }


    ]);
}());